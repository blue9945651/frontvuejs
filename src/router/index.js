import { createRouter, createWebHashHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/login',
    name: 'login',
    component: () => import('../views/Login.vue')
  },
  {
    path: '/agendas',
    name: 'agenda',
    component: () => import('../views/Agendas.vue')
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
