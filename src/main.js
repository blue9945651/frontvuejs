import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import VueSweetalert2 from 'vue-sweetalert2';
import DataTable from 'primevue/datatable';
import Column from 'primevue/column';

import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap";
import 'sweetalert2/dist/sweetalert2.min.css';
import 'primevue/resources/themes/saga-blue/theme.css'
import 'primevue/resources/primevue.min.css'
import 'primeicons/primeicons.css'

createApp(App)
    .use(router)
    .use(VueSweetalert2)
        .component('DataTable', DataTable)
        .component('Column', Column)
            .mount('#app')
